/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalpecha;

/**
 *
 * @author Maka
 */
public class InstrumentoCuerda extends Instrumento {
    private int cantidadCuerdas;

    public InstrumentoCuerda(String nombreCliente, String nombreInstrumento, int cantidadSemanas, int cantidadCuerdas) {
        super(nombreCliente, nombreInstrumento, cantidadSemanas);
        this.cantidadCuerdas = cantidadCuerdas;
    }

    public int getCantidadCuerdas() {
        return cantidadCuerdas;
    }

    @Override
    public float precioAlquiler() {
        float precio = 0;
        precio = (getCantidadSemanas() * 800 ) + (getCantidadCuerdas() * 300);
        return precio;
    }

}
