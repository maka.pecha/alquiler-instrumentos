/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalpecha;

import java.util.ArrayList;

/**
 *contenedora
 * @author Maka
 */
public class Alquiler {
    private static ArrayList<Instrumento> instrumento; 
    
    public Alquiler() {
        instrumento = new ArrayList();
    }
    
    public static ArrayList<Instrumento> getInstrumento() {
        return instrumento;
    }
    
    public void agregarInstrumento(Instrumento m){
        instrumento.add(m); 
    }
    
    public String ClienteAlquilerMayor()
    {
        int max = instrumento.get(0).getCantidadSemanas(); // Verifica valor de primer pos
        String nombre = instrumento.get(0).getNombreCliente();
        for (Instrumento i : instrumento) { // Recorro en la segunda posicion
            if (i.getCantidadSemanas() > max) { //Verifica que el valor sea mayor en la prox posicion.
                max = i.getCantidadSemanas();
                nombre = i.getNombreCliente();
            }
        }
        return nombre;
    }
    
    public float totalGanadoSinDesinfeccion(){
        float acumulador = 0;
        for (Instrumento i : instrumento) {
            if(i instanceof InstrumentoCuerda) {
                InstrumentoCuerda ic = (InstrumentoCuerda)i;
                acumulador += ic.precioAlquiler();
            } else if (i instanceof InstrumentoViento){
                InstrumentoViento iv = (InstrumentoViento)i;
                acumulador += iv.precioAlquilerSinDesinfeccion();
            }
        }
        return acumulador;
    }
    
    public float totalGanadoCuerdas() {
        float acumulador = 0;
        for (Instrumento i : instrumento) {
            if(i instanceof InstrumentoCuerda) {
                InstrumentoCuerda ic = (InstrumentoCuerda)i;
                acumulador += ic.precioAlquiler();
            } 
        }
        return acumulador;
    }
    
}
