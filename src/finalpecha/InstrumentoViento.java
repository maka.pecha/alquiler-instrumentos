/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalpecha;

/**
 *
 * @author Maka
 */
public class InstrumentoViento extends Instrumento{

    public InstrumentoViento(String nombreCliente, String nombreInstrumento, int cantidadSemanas) {
        super(nombreCliente, nombreInstrumento, cantidadSemanas);
    }

    @Override
    public float precioAlquiler() {
        float precio = 0;
        precio = (getCantidadSemanas() * 1000 ) + 500;
        return precio;
    }
    
    public float precioAlquilerSinDesinfeccion(){
        float precio = 0;
        precio = precioAlquiler() - 500;
        return precio;
    }
    
}
