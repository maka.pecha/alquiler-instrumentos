/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalpecha;

/**
 *clase base
 * @author Maka
 */
public abstract class Instrumento {
    private String nombreCliente;
    private String nombreInstrumento;
    private int cantidadSemanas;

    public Instrumento(String nombreCliente, String nombreInstrumento, int cantidadSemanas) {
        this.nombreCliente = nombreCliente;
        this.nombreInstrumento = nombreInstrumento;
        this.cantidadSemanas = cantidadSemanas;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public String getNombreInstrumento() {
        return nombreInstrumento;
    }

    public int getCantidadSemanas() {
        return cantidadSemanas;
    }

    @Override
    public String toString() {
        return "Instrumento{" + "nombreCliente=" + nombreCliente + ", nombreInstrumento=" + nombreInstrumento + ", cantidadSemanas=" + cantidadSemanas + '}';
    }

    public abstract float precioAlquiler();//obligo a las hijas a implementarlo

}
